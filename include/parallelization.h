#include <time.h>

inline void sleep_seconds(double x)
{
timespec ts;
ts.tv_sec = (int)x;
ts.tv_nsec = (x - (int)x) * 1e9;
nanosleep(&ts,0);
}